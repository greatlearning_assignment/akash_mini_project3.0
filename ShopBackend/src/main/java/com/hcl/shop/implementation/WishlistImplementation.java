package com.hcl.shop.implementation;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.hcl.shop.entity.Product;
import com.hcl.shop.entity.Users;
import com.hcl.shop.entity.WishlistProduct;
import com.hcl.shop.repository.ProductImple;
import com.hcl.shop.repository.UserRepository;
import com.hcl.shop.service.IWishlistService;
import com.hcl.shop.util.JwtGenerator;
@Service
public class WishlistImplementation implements IWishlistService {
	@Autowired
	private JwtGenerator generate;
	@Autowired
	private UserRepository userRepository;
	@Autowired
	private ProductImple productRepository;


	Users user=new Users();
	private  boolean notifyWishproducts;
	
	

	public boolean isNotifyWishproducts() {
		return notifyWishproducts;
	}

	public void setNotifyWishproducts(boolean notifyWishproducts) {
		this.notifyWishproducts = notifyWishproducts;
	}

	@Override
	@Transactional
	public List<WishlistProduct> addwishProduct(String token, long productId) {
	

				Long id = generate.parseJWT(token);
			System.out.println("-------------   hitting 1");
			Users user = userRepository.findById(id).get();
			if(user!=null) {
				System.out.println("-------------   hitting user ");
			Product product = productRepository.findById(productId).get();
			if(product!=null) {
			List<Product> products = null;
			for (WishlistProduct d : user.getWishlistProduct()) {
				products = d.getProductsList();
			}
			if (products == null) {
				Users userdetails = this.wishproducts(product,user);
				return userRepository.save(userdetails).getWishlistProduct();
			}
			
		
		
			
			Optional<Product> wishproduct = products.stream()
			.filter(t -> t.getProductId() == productId).findFirst();
			if (wishproduct.isPresent()) {
				throw null;
			} else {
				Users userdetails = this.wishproducts(product,user);
				return userRepository.save(userdetails).getWishlistProduct();
			}
			

			}//book
			}//user
			
			//write here exception........
			return null;
			
		
	
}
	
	public Users wishproducts(Product product,Users user) {
		
		WishlistProduct wishproduct =new WishlistProduct();
		ArrayList<Product> productlist = new ArrayList<>();
		productlist.add(product);
		wishproduct.setWishlistTime(LocalDateTime.now());
		wishproduct.setProductsList(productlist);
		user.getWishlistProduct().add(wishproduct);
		return user;
		
	}

	@Override
	@Transactional
	public List<WishlistProduct> getWishlistProducts(String token) {
		Long id;
	
			id = (long) generate.parseJWT(token);
			Users user = userRepository.findById(id).get();
			if(user!=null) {
			List<WishlistProduct> wishProducts = user.getWishlistProduct();
			List<WishlistProduct> productsinWish=new ArrayList<>();
			  for(WishlistProduct product:wishProducts) {
				  if(!(product.getProductsList().isEmpty())) {
					  productsinWish.add(product);
					   
					  
					  }
				  Product productstackdetails;
				 for( Product productstack:product.getProductsList()) {
				Long l=	 productstack.getNoOfProducts();
				int i=l.intValue();
				 if(i==0) {
					 productstackdetails=productstack;
					setNotifyWishproducts(false);
				 }
				 }

				  
				 
			  }
			
		     return productsinWish;
			}
			//write here exception........
		return null;
	}

	@Override
	@Transactional
	public boolean removeWishProduct(String token, Long productId) {
		
		Long id;
			id = (long) generate.parseJWT(token);
			Users user = userRepository.findById(id).get();
			if(user!=null) {
			Product product = productRepository.findById(productId).get();
			if(product!=null) {
	
			for (WishlistProduct  wishlist : user.getWishlistProduct()) {
				boolean exitsInWishlist = wishlist.getProductsList().stream()
						.noneMatch(products -> products.getProductId().equals(productId));
				if (!exitsInWishlist) {
					userRepository.save(user);
					wishlist.getProductsList().remove(product);
					wishlist.getProductsList().clear();
					boolean users = userRepository.save(user).getWishlistProduct()
					!= null ? true : false;
					if (user != null) {
						return users;
					}
				}
			}}//book
			//book exception
			
			}//user
			//exception user
		
		return false;
	}

	@Override
	@Transactional
	public int getCountOfWishlist(String token) {
		Long id;
			id = (long) generate.parseJWT(token);
         int countOfWishList=0;
		Users user = userRepository.findById(id).get();
		if(user!=null)
		{
		List<WishlistProduct> wishlist = user.getWishlistProduct();
         for(WishlistProduct wishproduct:wishlist) {
        	 if(!wishproduct.getProductsList().isEmpty()) {
        		 countOfWishList++;
        	 }
         }
		return countOfWishList;
		}
		//write here exception...................
		
		return 0;
		}

	
	
}