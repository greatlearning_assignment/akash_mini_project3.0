package com.hcl.shop.implementation;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.hcl.shop.entity.Product;
import com.hcl.shop.entity.Users;
import com.hcl.shop.exception.AdminNotFoundException;
import com.hcl.shop.exception.ProductNotFoundException;
import com.hcl.shop.repository.ProductImple;
import com.hcl.shop.repository.ProductInterface;
import com.hcl.shop.repository.CustomerRepository;
import com.hcl.shop.service.IAdminService;
import com.hcl.shop.util.JwtGenerator;

@Service
public class AdminServiceImpl implements IAdminService {

//	@Autowired
//	private IOrderStatusRepository orderStatusRepo;
//
	@Autowired
	CustomerRepository userRepo;

	@Autowired
	JwtGenerator jwt;

	@Autowired
	private ProductImple productRepository;
	
	@Autowired
	ProductInterface productRepo;

	@Override
	public boolean verifyProduct(long productId,String staus, String token) {

		long userid = 0;
		Users user = null;
			userid = jwt.parseJWT(token);
			System.out.println("user id:" + userid);
			user = userRepo.getCustomerDetailsbyId(userid);
			System.out.println("user:" + user);
	
		if (user != null) {
			Product product = productRepo.findByProductId(productId);
			System.out.println("productinfo "+product);
			
			if (product != null) {
				product.setStatus(staus);

				productRepo.save(product);
				return true;
				
			} else {
				throw new ProductNotFoundException("Product Not Found");
			}

		} else {
			throw new AdminNotFoundException("Admin Not Found");
		}
	}

		@Override
	public List<Product> getProductsByStatus(String status) {
		
		return productRepo.findByStatus(status);
	}

	


}
