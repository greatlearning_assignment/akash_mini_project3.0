package com.hcl.shop.implementation;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.hcl.shop.dto.CartDto;
import com.hcl.shop.entity.Product;
import com.hcl.shop.entity.CartItem;
import com.hcl.shop.entity.Quantity;
import com.hcl.shop.entity.Users;
import com.hcl.shop.entity.WishlistProduct;
import com.hcl.shop.exception.UserException;
import com.hcl.shop.repository.ProductImple;
import com.hcl.shop.repository.QuantityRepository;
import com.hcl.shop.repository.UserRepository;
import com.hcl.shop.service.ICartService;
import com.hcl.shop.service.IWishlistService;
import com.hcl.shop.util.JwtGenerator;

import lombok.extern.slf4j.Slf4j;

@Service
@Slf4j
public class CartServiceImplimentation implements ICartService {
	@Autowired
	private JwtGenerator generate;
	@Autowired
	private UserRepository userRepository;
	@Autowired
	private ProductImple productRepository;
	@Autowired
	private IWishlistService wishService;
	@Autowired
	private QuantityRepository quantityRepository;
	Users user = new Users();

	@Transactional
	@Override
	public List<CartItem> addProducttoCart(String token, long productId) {

		Long id;

		id = (long) generate.parseJWT(token);

		Users user = userRepository.findById(id).orElse(null);

		Product product = productRepository.findById(productId).get();
		if (product != null) {
			// if the book present in wishlist and book number is not equal to zero
			Long l = product.getNoOfProducts();
			int i = l.intValue();
//			log.info("-------hitting or not------------------------" + i);
			if (i > 0) {
//				log.info("-------hitting or not------------------------" + i);
				List<WishlistProduct> wishproduct = user.getWishlistProduct();
				for (WishlistProduct wishproducts : wishproduct) {

					boolean b = wishproducts.getProductsList().contains(product);
					if (b == true) {
						wishService.removeWishProduct(token, product.getProductId());
					}

				} // if book present in the wishbook

				List<Product> products = null;
				for (CartItem d : user.getCartProducts()) {
					products = d.getProductsList();
				}

				if (products == null) {
					Users userdetails = this.cartproducts(product, user);
					return userRepository.save(userdetails).getCartProducts();
				}
				/**
				 * Checking whether book is already present r not
				 */

				Optional<Product> cartproduct = products.stream().filter(t -> t.getProductId() == productId).findFirst();

				if (cartproduct.isPresent()) {
					throw null;
				} else {
					Users userdetails = this.cartproducts(product, user);
					return userRepository.save(userdetails).getCartProducts();
				}

			} // i==0

			throw new UserException("Out of stock u cannot add to cart");
		} // book
		return null;

	}

	public Users cartproducts(Product product, Users user) {
		long quantity = 1;
		CartItem cart = new CartItem();
		Quantity qunatityofproduct = new Quantity();
		ArrayList<Product> productlist = new ArrayList<>();
		productlist.add(product);
		cart.setCreatedTime(LocalDateTime.now());
		cart.setProductsList(productlist);
		ArrayList<Quantity> quantitydetails = new ArrayList<Quantity>();
		qunatityofproduct.setQuantityOfProduct(quantity);

		qunatityofproduct.setTotalprice(product.getPrice());

		quantitydetails.add(qunatityofproduct);
		cart.setQuantityOfProduct(quantitydetails);
		user.getCartProducts().add(cart);
		return user;
	}

	@Transactional
	@Override
	public List<CartItem> getProductsfromCart(String token) {
		Long id = (long) generate.parseJWT(token);
		Users user = userRepository.findById(id).get();
		if (user != null) {
			List<CartItem> cartItem = new ArrayList<>();
			for (CartItem cartProducts : user.getCartProducts()) {
				if (!(cartProducts.getProductsList().isEmpty())) {
					cartItem.add(cartProducts);
				}
			}
			return cartItem;
		} // user.

		return null;
	}

	@Transactional
	@Override
	public boolean removeProductsFromCart(String token, Long productId) {
		Long id;

		id = (long) generate.parseJWT(token);
		Users user = userRepository.findById(id).get();
		if (user != null) {
			Product product = productRepository.findById(productId).get();
			if (product != null) {
				Quantity quantity = quantityRepository.findById(id).orElseThrow(null);
				for (CartItem cartt : user.getCartProducts()) {
					boolean exitsProductInCart = cartt.getProductsList().stream()
							.noneMatch(products -> products.getProductId().equals(productId));
					if (!exitsProductInCart) {
						userRepository.save(user);
						cartt.getQuantityOfProduct().remove(quantity);
						cartt.getProductsList().remove(product);
						cartt.getQuantityOfProduct().clear();
						boolean users = userRepository.save(user).getCartProducts() != null ? true : false;
						if (user != null) {
							return users;
						}
					}

				}
			} // book
				// .book....exception here....
		} // user
		return false;

	}

	@Transactional
	@Override
	public int getCountOfProducts(String token) {
		Long id;

		id = (long) generate.parseJWT(token);
		int countOfProducts = 0;
		Users user = userRepository.findById(id).get();
		if (user != null) {
			List<CartItem> cartProducts = user.getCartProducts();
			for (CartItem cart : cartProducts) {
				if (!cart.getProductsList().isEmpty()) {
					countOfProducts++;
				}
			}
			return countOfProducts;
		} // user
			// ....write userwxception

		return 0;
	}

	@Transactional
	@Override
	public CartItem IncreaseProductsQuantityInCart(String token, Long productId, CartDto productQuantityDetails) {
		Long id;
		id = (long) generate.parseJWT(token);

		Long quantityId = productQuantityDetails.getQuantityId();
		Long quantity = productQuantityDetails.getQuantityOfProduct();
		Users user = userRepository.findById(id).get();
		if (user != null) {
			Product product = productRepository.findById(productId).get();
			if (product != null) {

				double totalprice = product.getPrice() * (quantity + 1);
				boolean notExist = false;
				for (CartItem cartt : user.getCartProducts()) {
					if (!cartt.getProductsList().isEmpty()) {
						notExist = cartt.getProductsList().stream().noneMatch(products -> products.getProductId().equals(productId));

						if (!notExist) {

//						

							Quantity quantityDetails = quantityRepository.findById(quantityId).orElseThrow(null);
							quantityDetails.setQuantityOfProduct(quantity + 1);
							quantityDetails.setTotalprice(totalprice);
							if (quantityDetails.getQuantityOfProduct() <= product.getNoOfProducts()) {
								quantityRepository.save(quantityDetails);
								return cartt;
							}
							throw new UserException("there is no enough quantity of product");

						}

					} // cart
				}

			} // book

		} // user

		return null;
	}

	@Transactional
	@Override
	public CartItem decreaseProductsQuantityInCart(String token, Long productId, CartDto productQuantityDetails) {
		Long id;

		id = (long) generate.parseJWT(token);
		Long quantityId = productQuantityDetails.getQuantityId();
		Long quantity = productQuantityDetails.getQuantityOfProduct();

		Users user = userRepository.findById(id).get();
		if (user != null) {
			Product product = productRepository.findById(productId).get();
			if (product != null) {
				double totalprice = product.getPrice() * (quantity - 1);
				boolean notExist = false;
				for (CartItem cartt : user.getCartProducts()) {
					if (!cartt.getProductsList().isEmpty()) {
						notExist = cartt.getProductsList().stream().noneMatch(products -> products.getProductId().equals(productId));
						if (!notExist) {

							Quantity quantityDetails = quantityRepository.findById(quantityId).orElseThrow(null);
							quantityDetails.setQuantityOfProduct(quantity - 1);
							quantityDetails.setTotalprice(totalprice);
							Long l = quantityDetails.getQuantityOfProduct();
							int i = l.intValue();
							if (i >= 0) {
								if (i == 0) {
									removeProductsFromCart(token, productId);
								}
								quantityRepository.save(quantityDetails);
								return cartt;
							}
							throw new UserException("invalid Quantity");
						}

					}
				}
			} // book

		} // user
		return null;

	}

}