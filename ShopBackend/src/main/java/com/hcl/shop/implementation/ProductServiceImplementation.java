package com.hcl.shop.implementation;

import java.time.LocalDateTime;
import java.util.Collections;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import javax.transaction.Transactional;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

import com.hcl.shop.dto.ProductDto;
import com.hcl.shop.dto.EditProductDto;
import com.hcl.shop.dto.RatingReviewDTO;
import com.hcl.shop.entity.Product;
import com.hcl.shop.entity.ReviewAndRating;
import com.hcl.shop.entity.Users;
import com.hcl.shop.exception.ProductAlreadyExist;
import com.hcl.shop.exception.UserException;
import com.hcl.shop.repository.AddressRepository;
import com.hcl.shop.repository.ProductImple;
import com.hcl.shop.repository.IUserRepository;
import com.hcl.shop.repository.ReviewRatingRepository;
import com.hcl.shop.response.EmailData;
import com.hcl.shop.service.IProductService;
import com.hcl.shop.util.EmailProviderService;
import com.hcl.shop.util.JwtGenerator;

import lombok.extern.slf4j.Slf4j;

@Slf4j
@Service
public class ProductServiceImplementation implements IProductService {
	private Product productinformation = new Product();
	private ModelMapper modelMapper = new ModelMapper();
	
	@Autowired
	private EmailProviderService em;
	@Autowired
	private EmailData emailData;

	@Autowired
	private ProductImple repository;

	@Autowired
	private IUserRepository userRepository;
	@Autowired
	AddressRepository addrepository;

	@Autowired
	private JwtGenerator generate;

	
	@Autowired
	private ReviewRatingRepository rrRepository;

    @Autowired
    private  WishlistImplementation WishServiceNotify;
	
	@Transactional
	@Override

	public boolean addProducts(String imageName,ProductDto information,String token)
	{	
		Long id;
	
			id = (long) generate.parseJWT(token);
			Users userInfo = userRepository.getUserById(id);
			if(userInfo != null) 
			{			
				String userRole = userInfo.getRole();
				System.out.println("actual Role is " + userRole);
				String fetchRole = userRole;
				if (fetchRole.equals("seller") ) 
				{
					Product product=repository.fetchbyProductName(information.getProductName());
					System.out.println("Product name "+information.getProductName());
					if(product ==null)
					{
						productinformation = modelMapper.map(information, Product.class);
						productinformation.setProductName(information.getProductName());
						productinformation.setProductAuthor(information.getProductName());
						productinformation.setPrice(information.getPrice());  
						productinformation.setImage(imageName);
						productinformation.setStatus("OnHold");
						productinformation.setNoOfProducts(information.getNoOfProducts());
						productinformation.setCreatedDateAndTime(LocalDateTime.now());
						productinformation.setUserId(id);
						repository.save(productinformation);
						return true;
					}
					else
					{
						throw new ProductAlreadyExist("Product is already exist Exception..");
					}
				}
				else 
				{
					throw new UserException("Your are not Authorized User");
				}
			
		} else {
			throw new UserException("User doesn't exist");
		}

	}

	@Transactional
	@Override
	public List<Product> getProductInfo(String token) {
		Long id;

		id = (long) generate.parseJWT(token);
		Users userInfo = userRepository.getUserById(id);
		if (userInfo != null) {
			List<Product> products = repository.getAllProducts(id);
			return products;
		} else {
			throw new UserException("User doesn't exist");
		}

	}

	public double getOriginalPrice(double price, long quantity) {
		long result = (long) (price / quantity);
		return result;
	}

	@Override
	public Product getTotalPriceofProduct(Long productId, long quantity) {
		Product productinfo = repository.fetchbyId(productId);
		double Price = productinfo.getPrice();

		long Quantity = quantity;

		if (Quantity <= productinfo.getNoOfProducts() || Quantity >= productinfo.getNoOfProducts()) {
			if (productinfo != null && quantity > 0) {
				double price = getOriginalPrice(Price, productinfo.getNoOfProducts());
				double totalPrice = (price * Quantity);
				productinfo.setNoOfProducts(quantity);

				productinfo.setNoOfProducts(quantity);

				productinfo.setPrice(totalPrice);
				repository.save(productinfo);
				return productinfo;
			} else if (productinfo != null && quantity < 1) {
				double price = getOriginalPrice(Price, productinfo.getNoOfProducts());
				double totalPrice = (price * 1);
				productinfo.setNoOfProducts(quantity);
				productinfo.setPrice(totalPrice);
				repository.save(productinfo);
				return productinfo;
			}
		}
		return null;
	}

	@Transactional
	@Override
	public List<Product> sortGetAllProducts() {
		List<Product> list = repository.findAll();
		list.sort((Product product1, Product product2) -> product1.getCreatedDateAndTime().compareTo(product2.getCreatedDateAndTime()));
		return list;
	}

	@Override
	public List<Product> sorting(boolean value) {
		List<Product> list = repository.findAll();
		if (value == true) {
			list.sort((Product product1, Product product2) -> product1.getPrice().compareTo(product2.getPrice()));
			return list;
		} else {
			list.sort((Product product1, Product product2) -> product1.getPrice().compareTo(product2.getPrice()));
			Collections.reverse(list);
			return list;
		}
	}

	@Override
	public List<Product> findAllPageBySize(int pagenumber) {
		long count = repository.count();
		int pageSize = 2;
		int pages = (int) ((count / pageSize));
		int i = pagenumber; // i should start with zero or 0...
		while (i <= pages) {
			List<Product> list = repository.findAllPage(PageRequest.of(i, pageSize));
			i++;
			return list;
		}
		return null;
	}

	@Override
	public Product getProductbyId(Long productId) {
		Product info = repository.fetchbyId(productId);
		if (info != null) {
			return info;
		}
		return null;
	}

	@Override

	public boolean editProduct(long productId,EditProductDto information,String token) {
		
		Long id;
	
			id = (long) generate.parseJWT(token);
			Users userInfo = userRepository.getUserById(id);
			if(userInfo != null) 
			{			
				String userRole = userInfo.getRole();
				System.out.println("actual Role is " + userRole);
				String fetchRole = userRole;

				if (fetchRole.equals("seller") ) 
				{
					Product info =repository.fetchbyId(productId);
					if(info!=null) 
					{
						Long l=info.getNoOfProducts();
						int beforeNoOfproducts=l.intValue();
//						log.info("------------------------"+beforeNoOfbooks);
						info.setProductId(productId);
						info.setProductName(information.getProductName());
						info.setNoOfProducts(information.getNoOfProducts());
						info.setPrice(information.getPrice());
						info.setProductAuthor(information.getProductAuthor());
						info.setProductDetails(information.getProductDetails());
//						info.setImage(imageName);
						info.setUpdatedDateAndTime(information.getUpdatedAt());
					
						Long af=info.getNoOfProducts();
						int afterNoOfproducts=af.intValue();
//						log.info("------------------------"+afterNoOfbooks);
//						if(after==before) {
//						
//						}
						
//						for (WishlistBook w : userInfo.getWishlistBook()) {
//							for(Book wishbook :w.getBooksList()) {
//						
//							if(wishbook.getBookId()==bookId) {
						if(beforeNoOfproducts==0) {
//							log.info("------------------------"+afterNoOfbooks);
							
							if(afterNoOfproducts>beforeNoOfproducts) {
								WishServiceNotify.setNotifyWishproducts(true);
								if(WishServiceNotify.isNotifyWishproducts()==true) {
//									Users userdetails=new Users();
			
//									emailData.setEmail(userdetails.getEmail());
									
									String body="<html> \n"
							 				
								 			
	 				+"<h3 ; style=\"background-color:#990000;color:#ffffff;\" >\n "
	 				+ "<center>Shop Notification</center> "
	 				+ "</h3>\n "
	 				+ "<body  style=\"background-color:#FAF3F1;\">\n"+
	 				"<br>"+" Your Wish product is available name is"+info.getProductName()+"\n"
	 				+"   check ur product below link<br>"+"\n"
	 		+" http://localhost:4200/wish<br>"
	
	 		+ "</body>"
	 		+ " </html>" ;
											
											emailData.setSubject("Notification in WishList");
									
											emailData.setBody(body);
									
											em.sendMail("", 
													emailData.getSubject(), emailData.getBody());
									 
								}
							}
							
						}
//							}//if id equating
//							}//wish book
//						}//wishbookw for
						info.setUpdatedDateAndTime(LocalDateTime.now());
						repository.save(info);
						return true;
					}
				}
				else 
				{
					throw new UserException("Your are not Authorized User");
				}
			}
		 else {
			throw new UserException("User doesn't exist");
		}

		return false;
	}

	@Transactional
	@Override
	public boolean deleteProduct(long productId, String token) {
		Long id;

		id = (long) generate.parseJWT(token);
		Users userInfo = userRepository.getUserById(id);
		if (userInfo != null) {
			String userRole = userInfo.getRole();
			System.out.println("actual Role is " + userRole);
//			log.info("Actual ");
			String fetchRole = userRole;

			if (fetchRole.equals("seller") ) {
				Product info = repository.fetchbyId(productId);
				if (info != null) {
					repository.deleteByProductId(productId);
					return true;
				}
			} else {
				throw new UserException("Your are not Authorized User");
			}
		} else {
			throw new UserException("User doesn't exist");
		}

		return false;
	}


	@Transactional
	@Override
	public boolean editProductStatus(long productId, String status, String token) {
		Long id;

		id = (long) generate.parseJWT(token);
		Users userInfo = userRepository.getUserById(id);
//		log.info("");
		if (userInfo != null) {
			Product info = repository.fetchbyId(productId);
			if (info != null) {
				repository.updateProductStatusByProductId(status, productId);
				return true;
			}
		} else {
			throw new UserException("User doesn't exist");
		}

		return false;
	}

	@Transactional
	@Override
	public List<Product> getAllOnHoldProducts(String token) {
		Long id;

		id = (long) generate.parseJWT(token);
		Users userInfo = userRepository.getUserById(id);
		if (userInfo != null) {
			List<Product> approvedOnHoldProducts = repository.getAllOnHoldProducts();
			return approvedOnHoldProducts;
		} else {
			throw new UserException("User doesn't exist");
		}

	}

	public List<Product> getAllRejectedProducts(String token) {
		Long id;

		id = (long) generate.parseJWT(token);
		Users userInfo = userRepository.getUserById(id);
		if (userInfo != null) {
			List<Product> rejectedProducts = repository.getAllRejectedProducts();
			return rejectedProducts;
		} else {
			throw new UserException("User doesn't exist");
		}

	}

	/**
	 * This controller is for getting 12 approval books per page! it can search book
	 * based on there autherName it can sort the book by anything like price,
	 * book_name, book_id etc, it can order the book both asc and desc order default
	 * will be desc order it can return the book based on there passing url
	 * paramater
	 * 
	 * @param searchByBooKName example (" ", book name, raju, etc)
	 * @param page             Example (" ", 1,2,3,4.........)
	 * @param sortBy           example (" ", book_id, price, created_date_and_time
	 *                         etc)
	 * @param order            (" ", asc,desc,)
	 * @return 12 books and number of page and everything
	 */
	@Override
	public Page<Product> getProductAproval(Optional<String> searchBy, Optional<Integer> page, Optional<String> sortBy,
			Optional<String> order) {
		if (order.equals(Optional.ofNullable("asc"))) {
			return repository.findByProductName(searchBy.orElse("_"),
					PageRequest.of(page.orElse(0), 12, Sort.Direction.ASC, sortBy.orElse("product_id")));
		} else {
			return repository.findByProductName(searchBy.orElse("_"),
					PageRequest.of(page.orElse(0), 12, Sort.Direction.DESC, sortBy.orElse("product_id")));
		}
	}

	@Override
	public List<Product> getAllAprovedProduct() {
		List<Product> approvedProducts = repository.getAllApprovedProducts();
		return approvedProducts;
	}


	@Override
	public boolean writeReviewAndRating(String token, RatingReviewDTO rrDTO, Long productId) {
//		Long uId = generate.parseJWT(token);
//		Users user = userRepository.getUserById(uId);
//		Book book = repository.fetchbyId(bookId);
//		boolean notExist =  book.getReviewRating().stream().noneMatch(rr -> rr.getUser().getUserId()==uId);
//		if(notExist) {
//			ReviewAndRating rr = new ReviewAndRating(rrDTO);
//			rr.setUser(user);
//			book.getReviewRating().add(rr);
//			rrRepository.save(rr);
//			repository.save(book);
//		}
//		else {
//			ReviewAndRating rr = book.getReviewRating().stream().filter(r -> r.getUser().getUserId()==uId).findFirst().orElseThrow(() -> new BookAlreadyExist("Review doesnot exist"));
//			rr.setRating(rrDTO.getRating());
//			rr.setReview(rrDTO.getReview());
//			rrRepository.save(rr);
//			repository.save(book);
//
//		}
		
		Long userId = generate.parseJWT(token);
		Users user = userRepository.getUserById(userId);
		ReviewAndRating review = rrRepository.getProductReview(productId , user.getName());
		if(review==null) {
			ReviewAndRating rr = new ReviewAndRating(rrDTO);
			rr.setProductId(productId);
			rr.setUserName(user.getName());
			rrRepository.save(rr);
			return true;
			
		}
		return false;

	}

	@Override
	public List<ReviewAndRating> getRatingsOfProduct(Long productId) {

//		Book book=repository.fetchbyId(bookId);
//
//		return book.getReviewRating();
		return rrRepository.getreviews(productId);
	}
	
	@Override
	public double avgRatingOfProduct(Long productId) {
		double rate=0.0;
		try {
		rate = repository.avgRateOfProduct(productId);
		System.out.println("rate getted:"+rate);
		}catch(Exception e)
		{
			System.out.println("No rating");
		}
		return rate;
	}

	@Override
	public Integer getProductsCount() {
		
		return repository.getAllApprovedProducts().size();
	}
	
	@Transactional
	@Override
	public boolean uploadProductImage(long productId, String imageName, String token) {
		Long id;

		id = (long) generate.parseJWT(token);
		Users userInfo = userRepository.getUserById(id);
		if (userInfo != null) {
			String userRole = userInfo.getRole();
			System.out.println("actual Role is " + userRole);
			String fetchRole = userRole;

			if (fetchRole.equals("seller") ) {
				Product info = repository.fetchbyId(productId);
				if (info != null) {
					info.setImage(imageName);
					repository.save(info);
					return true;
				}
			}
		} else {
			throw new UserException("User doesn't exist");
		}

		return false;
	}
	
	@Transactional
	@Override
	public List<Product> sortProductByRate() {
		
		List<Product> products = repository.getAllApprovedProducts();
		System.out.println("Approved products:"+products);
		List<Product> sortProduct = products.stream().sorted((product1,product2)->(avgRatingOfProduct(product1.getProductId())<avgRatingOfProduct(product2.getProductId()))?1:(avgRatingOfProduct(product1.getProductId())>avgRatingOfProduct(product2.getProductId()))?-1:0).collect(Collectors.toList());
		System.out.println("After sorting:"+sortProduct);
		return sortProduct;
	}

}
