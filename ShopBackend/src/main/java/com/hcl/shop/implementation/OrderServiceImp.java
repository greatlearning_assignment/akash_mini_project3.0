package com.hcl.shop.implementation;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.hcl.shop.entity.Product;
import com.hcl.shop.entity.CartItem;
import com.hcl.shop.entity.Order;
import com.hcl.shop.entity.Quantity;
import com.hcl.shop.entity.Users;
import com.hcl.shop.exception.UserException;
import com.hcl.shop.repository.ProductImple;
import com.hcl.shop.repository.OrderRepository;
import com.hcl.shop.repository.UserRepository;
import com.hcl.shop.response.EmailData;
import com.hcl.shop.service.ICartService;
import com.hcl.shop.service.IOrderServices;
import com.hcl.shop.util.EmailProviderService;
import com.hcl.shop.util.JwtGenerator;

import lombok.extern.slf4j.Slf4j;

@Service
@Slf4j
public class OrderServiceImp implements IOrderServices {
	@Autowired
	private JwtGenerator generate;
	
	@Autowired
	private ICartService cartservice;

	@Autowired
	private UserRepository userRepo;
	@Autowired
	private ProductImple productRepository;

	@Autowired
	private EmailProviderService em;
	@Autowired
	private EmailData emailData;

	@Autowired
	OrderRepository orderRepository;

	public Order placeOrder(String token, Long productId, Long addressId) {
		Long id = generate.parseJWT(token);
		Users userdetails = userRepo.findById(id).get();
		if (userdetails != null) {
			Order orderDetails = new Order();
			Random random = new Random();
			ArrayList<Product> list = new ArrayList<>();
			ArrayList<Quantity> quantitydetails = new ArrayList<>();
			ArrayList<String> details = new ArrayList<>();
			/*
			 * getting user cartbooks to order as a list userdetail.getcatbook return type
			 * is List
			 */
			List<CartItem> cartproduct = userdetails.getCartProducts();
//			log.info("------------------------------cartbook------1--"+cartbook);
			/*
			 * now iterating specific cart book
			 */
			List<Product> userCartproducts = null;
			for (CartItem userCart : cartproduct) {
				// select specific cart book to order
//				log.info("-----------------------usercart-------------2--"+userCart);
				userCartproducts = userCart.getProductsList();// getting books from cart
				for (Product product : userCartproducts) {
//					log.info("-----------------------book---------------"+book);
					if (product.getProductId().equals(productId))// specific book to order @path taking input of this api
					{
						/*
						 * decreasing the quantity of book
						 */
						long orderId;
						for (Quantity productquantity : userCart.getQuantityOfProduct()) {
							// List q=userCart.getQuantityOfBook();
//							log.info("-----------------------QUantity-------------2--"+bookquantity);
							Long noOfProducts = product.getNoOfProducts() - productquantity.getQuantityOfProduct();
							product.setNoOfProducts(noOfProducts);
							Product bb = productRepository.save(product);
							try {
								list.add(bb);
								orderId = random.nextInt(1000000);
								if (orderId < 0) {
									orderId = orderId * -1;
								}
								double totalprice = product.getPrice() * (productquantity.getQuantityOfProduct());
//								log.info("-----------------------QUantity-------------2--"+bookquantity.getQuantityOfBook());
								orderDetails.setTotalPrice(totalprice);
								quantitydetails.add(productquantity);
								orderDetails.setOrderId(orderId);
								orderDetails.setQuantityOfProducts(quantitydetails);
								orderDetails.setOrderPlacedTime(LocalDateTime.now());
								orderDetails.setOrderStatus("pending");
								orderDetails.setAddressId(addressId);
								orderDetails.setProductsList(list);
								details.add("orderId:" + orderId + "\n" + "ProductName:"+ product.getProductName() + "\n"
										+ "Quantity:" + productquantity.getQuantityOfProduct() + "\n" + "TotalPrice:"
										+ productquantity.getTotalprice());
							} catch (Exception e) {
								throw new UserException("order Failed");
							}
						}//quantity for
					} // if condition checks id of the books

				} // book iteration--for
			} // cart iterate--for
			userdetails.getOrderProductDetails().add(orderDetails);
			String data = "";
			for(String dt:details) {
				data=data+dt;	
//				log.info("\n "+dt);
			}    
			
			Product product = productRepository.findById(productId).orElse(null);
	 		String body="<html> \n"
	 				
	 			
	 				+"<h3 ; style=\"background-color:#990000;color:#ffffff;\" >\n "
	 				+ "<center>HomeCart</center> "
	 				+ "</h3>\n "
	 				+ "<body  style=\"background-color:#FAF3F1;\">\n"+
	 				"<img src=\"E:\\git merge ideation\\final front\\BookStoreFrontend\\src\\assets\\bookimage/"
	 				+product.getImage()+ "\" alt=\"productImage\">"
	 				
	 			 +userdetails.getEmail()+
	 				" <br>"+"order details <br>"+" \n"+data+"\n"
	 				+"please rate us below link<br>"+"\n"
	 		+"http://localhost:4200/products/ratingreview<br>"
	
	 		+ "</body>"
	 		+ " </html>" ;
			emailData.setEmail(userdetails.getEmail());
	
			emailData.setSubject("your Order is succefully placed");
	
			emailData.setBody(body);

	
			em.sendMail(emailData.getEmail(), emailData.getSubject(), emailData.getBody());	
			//em.sendMail(emailData.getEmail(), emailData.getSubject(), emailData.getBody());

			System.out.println("emailData.getEmail() "+emailData.getEmail());
			System.out.println("emailData.getSubject() "+emailData.getSubject());
			System.out.println("emailData.getBody() "+emailData.getBody());
			em.sendMail(emailData.getEmail(), emailData.getSubject(), emailData.getBody());
			System.out.println("rate mail sent after order");
			/*
			 * remove specific book from the cart........
			 */

			if (cartservice.removeProductsFromCart(token, productId)) {
				userRepo.save(userdetails);
				return orderDetails;
			}
		}

		throw new UserException("user not found ");
	}

	@Override
	public boolean confirmProducttoOrder(String token, Long productId) {
		Long id = generate.parseJWT(token);
		Users userdetails = userRepo.findById(id).orElseThrow(null);

		for (Order order : userdetails.getOrderProductDetails()) {
			boolean notExist = order.getProductsList().stream().noneMatch(products -> products.getProductId().equals(productId));
                             
			if (!notExist) {
				return true;
			}
		}
		return false;
	}
	
	
	@Transactional
	@Override
	public int getCountOfProducts(String token) {
 		Long id = generate.parseJWT(token);
		int countOfProducts = 0;
		Users userdetails = userRepo.findById(id)
				.orElseThrow(null);

		List<Order> products = userdetails.getOrderProductDetails();
		for (Order cart : products) {
			if (!cart.getProductsList().isEmpty()) {

				countOfProducts++;
			}
		}
		return countOfProducts;
	}
	
	
	
	@Transactional
	@Override
	public List<Order> getOrderList(String token) {
		long id = generate.parseJWT(token);
		Users userdetails = userRepo.findById(id)
				.orElseThrow(null);

		return userdetails.getOrderProductDetails();

	}



	@Transactional
	@Override
	public int changeOrderStatus(String status,long orderId) {

		int changedOrderStatus = orderRepository.OrderStatusdefault(status,orderId);
//		long userId=orderRepository.findUserId(orderId);
//		
//		
//		Users userdetails = userRepo.findById(userId).get();
//		
//		
//		
//		if(changedOrderStatus >0) 
//		{	 
//			String body="";
//				emailData.setEmail(userdetails.getEmail());		
//				emailData.setSubject("Book Store");
//				body=(status.equals("in shipment")) ? "Your Order has been Shipped" : (status.equals("delivered")) ? "Your Order has been Delivered" : "Your order is in Progress"; 
//				
//				emailData.setBody(body);
//				em.sendMail(emailData.getEmail(), emailData.getSubject(), emailData.getBody());
//		}
		return changedOrderStatus;
	}
	

	public String getstatusresult()
	{
		return null;
		
	}

	public List<Order> getallOrders() {

		List<Order> orderIds = orderRepository.getorder();
		return orderIds;
	}


	@Override
	public List<Order> getInProgressOrders() {
		List<Order> inProgressOrder = orderRepository.getInProgressOrder();
		return inProgressOrder;
	}
	
}