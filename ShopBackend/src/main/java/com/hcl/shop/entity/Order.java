
package com.hcl.shop.entity;

import java.io.Serializable;
import java.time.LocalDateTime;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToMany;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import lombok.Data;

@Entity
@Data
@Table(name="order_details")
public class Order implements Serializable{

	private static final long serialVersionUID = 1L;
	@Id
	private Long orderId;
	@Column(name = "order_placed_time")
	private LocalDateTime orderPlacedTime;
	


	private String orderStatus;
	
	private Double totalPrice;
	
	private Long addressId;

	
	@OneToMany
	(cascade = CascadeType.ALL, targetEntity = Quantity.class)
	@JoinColumn(name = "orderId")
	private List<Quantity> QuantityOfProducts;
	
	@ManyToMany(cascade = CascadeType.ALL)
	private List<Product> ProductsList;
	 public Order() {
		 
	 }
	

	public Order(Long orderId, LocalDateTime orderPlacedTime, List<Quantity> quantityOfBooks,
			List<Product> booksList) {
		super();
		this.orderId = orderId;
		this.orderPlacedTime = orderPlacedTime;
		QuantityOfProducts = quantityOfBooks;
		ProductsList = booksList;
	}


	public Long getOrderId() {
		return orderId;
	}


	public void setOrderId(Long orderId) {
		this.orderId = orderId;
	}


	public LocalDateTime getOrderPlacedTime() {
		return orderPlacedTime;
	}


	public void setOrderPlacedTime(LocalDateTime orderPlacedTime) {
		this.orderPlacedTime = orderPlacedTime;
	}


	public String getOrderStatus() {
		return orderStatus;
	}


	public void setOrderStatus(String orderStatus) {
		this.orderStatus = orderStatus;
	}


	public Double getTotalPrice() {
		return totalPrice;
	}


	public void setTotalPrice(Double totalPrice) {
		this.totalPrice = totalPrice;
	}


	public Long getAddressId() {
		return addressId;
	}


	public void setAddressId(Long addressId) {
		this.addressId = addressId;
	}


	public List<Quantity> getQuantityOfProducts() {
		return QuantityOfProducts;
	}


	public void setQuantityOfProducts(List<Quantity> quantityOfBooks) {
		QuantityOfProducts = quantityOfBooks;
	}


	public List<Product> getProductsList() {
		return ProductsList;
	}


	public void setProductsList(List<Product> booksList) {
		ProductsList = booksList;
	}


	public static long getSerialversionuid() {
		return serialVersionUID;
	}
	


}