package com.hcl.shop;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ComponentScan;
@ComponentScan({"com.hcl.shop"})


@SpringBootApplication
public class ShopForHomeApplication {

	public static void main(String[] args) {
		SpringApplication.run(ShopForHomeApplication.class, args);
	}
}
