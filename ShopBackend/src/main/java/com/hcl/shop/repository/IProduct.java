package com.hcl.shop.repository;

import java.util.List;

import com.hcl.shop.entity.Product;

public interface IProduct {

	Product save(Product productinformation);

	List<Product> getUsers();

}
