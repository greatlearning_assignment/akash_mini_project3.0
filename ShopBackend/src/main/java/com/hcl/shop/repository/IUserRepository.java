/**
 * 
 */
package com.hcl.shop.repository;

import java.util.List;

import com.hcl.shop.entity.Users;
import com.hcl.shop.request.PasswordUpdate;

/**
 * @author HP
 *
 */
public interface IUserRepository {
	Users save(Users users);

	Users getUser(String email);

	boolean verify(Long id);

	boolean upDate(PasswordUpdate information, Long token);

	Users getUserById(Long id );

	List<Users> getUsers();

}
