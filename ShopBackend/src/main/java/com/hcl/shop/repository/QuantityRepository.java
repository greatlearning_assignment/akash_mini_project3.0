package com.hcl.shop.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.hcl.shop.entity.Quantity;
@Repository
public interface QuantityRepository extends JpaRepository<Quantity, Long>{

}