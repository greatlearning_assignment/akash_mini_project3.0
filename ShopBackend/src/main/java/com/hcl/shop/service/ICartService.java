package com.hcl.shop.service;

import java.util.List;

import com.hcl.shop.dto.CartDto;
import com.hcl.shop.entity.CartItem;

public interface ICartService {
 List<CartItem> addProducttoCart(String token,long productId);
 
 List<CartItem> getProductsfromCart(String token);
 
 boolean removeProductsFromCart(String token, Long productId);
 
 int getCountOfProducts(String token);
 
 CartItem IncreaseProductsQuantityInCart(String token, Long productId, CartDto productQuantityDetails);
 
 CartItem decreaseProductsQuantityInCart(String token, Long productd, CartDto productQuantityDetails);

 
}
