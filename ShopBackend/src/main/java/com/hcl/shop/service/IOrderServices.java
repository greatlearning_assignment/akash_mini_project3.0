package com.hcl.shop.service;


import java.util.List;

import com.hcl.shop.entity.Order;


public interface IOrderServices {
	
    boolean confirmProducttoOrder(String token, Long productd);
    
	Order  placeOrder(String token ,Long productId, Long addressId);

	

	int getCountOfProducts(String token);
	
	 List<Order> getOrderList(String token);

	
	List<Order> getallOrders();

	int changeOrderStatus(String status,long orderId);

	List<Order> getInProgressOrders();



	
}