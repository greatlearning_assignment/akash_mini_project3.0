package com.hcl.shop.service;

import java.util.List;

import com.hcl.shop.entity.Product;

public interface IAdminService {
	

	boolean verifyProduct(long bookId, String staus, String token);


	List<Product> getProductsByStatus(String status);


}
