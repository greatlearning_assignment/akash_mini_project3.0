/**
 * 
 */
package com.hcl.shop.service;

import java.util.List;

import com.hcl.shop.dto.UserDto;
import com.hcl.shop.entity.Users;
import com.hcl.shop.request.LoginInformation;
import com.hcl.shop.request.PasswordUpdate;

/**
 * @author HP
 *
 */
public interface UserServices {

	Users login(LoginInformation information);
	boolean register(UserDto ionformation);
	boolean verify(String token) throws Exception;
	boolean isUserExist(String email);
	boolean update(PasswordUpdate information, String token);
	List<Users> getUsers();
	Users getSingleUser(String token);
}