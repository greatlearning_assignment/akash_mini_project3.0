package com.hcl.shop.service;

import java.util.List;

import org.springframework.stereotype.Repository;

import com.hcl.shop.dto.AddressDto;
import com.hcl.shop.dto.UpdateAddressDto;
import com.hcl.shop.entity.Address;
import com.hcl.shop.entity.Users;

@Repository
public interface IAdressService {

	Address addAddress(AddressDto address, String token);

	Users deleteAddress(String token, Long addressId);

	Address updateAddress(UpdateAddressDto address, String token);

	List<Address> getAllAddress();

	Address getAddress(Long id);

	List<Address> getAddressByUserId(String token);

	Address getAddress(String type, String token);

}
