package com.hcl.shop.service;

import java.util.List;

import com.hcl.shop.entity.Order;

public interface OrderService {

	List<Order> getOrderList(String token);

	Order getOrderConfrim(String token);

	int getCountOfProducts(String token);
}
