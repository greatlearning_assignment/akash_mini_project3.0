package com.hcl.shop.service;

import java.util.List;

import com.hcl.shop.entity.WishlistProduct;

public interface IWishlistService {
	List<WishlistProduct> addwishProduct(String token,long productId);
	
	List<WishlistProduct> getWishlistProducts(String token);
	
	boolean removeWishProduct(String token, Long productId);
	
	int getCountOfWishlist(String token);

}
