package com.hcl.shop.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.hcl.shop.dto.CartDto;
import com.hcl.shop.entity.CartItem;
import com.hcl.shop.response.Response;
import com.hcl.shop.service.ICartService;

@RestController
@CrossOrigin
public class CartController {

	@Autowired
	private ICartService cartService;
	@PostMapping("shop/v3/cart/addproductCart/{productId}")
	public ResponseEntity<Response> addProductsToCart(@RequestHeader String token,@PathVariable long productId) throws Exception {
	    List<CartItem> cartItem = cartService.addProducttoCart(token,productId);
	    return ResponseEntity.status(HttpStatus.ACCEPTED)
				.body(new Response("product added to cart", 200,cartItem));
	  	}
	

	@GetMapping("shop/v3/cart/getcartproducts")
	public ResponseEntity<Response> getProductsfromCart(@RequestHeader(name="token")  String token) throws Exception {
		    List<CartItem> cartdetails = cartService.getProductsfromCart(token);
		    return ResponseEntity.status(HttpStatus.ACCEPTED)
					.body(new Response("products fetched from cart", 200,cartdetails));
	}

	
	@DeleteMapping("shop/v3/cart/removeCartProducts/{productId}")
	public ResponseEntity<Response> removeProductsToCart(@RequestHeader(name="token") String token ,@PathVariable Long productId) throws Exception {
		System.out.println("jjjj"+ token + productId);
		boolean cartdetails = cartService.removeProductsFromCart(token,productId);
		return ResponseEntity.status(HttpStatus.ACCEPTED).body(new Response("product removed from cart", 200,cartdetails));  				
	}
	
	@GetMapping("shop/v3/cart/productCount")
	public ResponseEntity<Response> getProductsCount(@RequestHeader(name="token") String token) throws Exception {
		    int cartdetails = cartService.getCountOfProducts(token);
		    return ResponseEntity.status(HttpStatus.ACCEPTED).body(new Response("Count of to product in cart", 200,cartdetails));
	}
	
	@PutMapping("shop/v3/cart/increaseproductsquantity")
	public ResponseEntity<Response> increaseProductsQuantity(@RequestHeader String token,@RequestParam Long productId,
			@RequestBody CartDto productQuantityDetails) throws Exception {
		CartItem cartdetails = cartService.IncreaseProductsQuantityInCart(token, productId,productQuantityDetails);
	    return ResponseEntity.status(HttpStatus.ACCEPTED).body(new Response("increased 1 quantity of product ", 200,cartdetails));   	
		
	}
	
	@PutMapping("shop/v3/cart/decreaseQuantityPrice")
	public ResponseEntity<Response> descreaseProductsQuantity(@RequestHeader(name="token") String token,@RequestParam("productId") Long productId,@RequestBody CartDto productQuantityDetails) throws Exception {
		CartItem cartdetails = cartService.decreaseProductsQuantityInCart(token, productId, productQuantityDetails);
		  return ResponseEntity.status(HttpStatus.ACCEPTED).body
				  (new Response("decreased 1 quantity of product ", 200,cartdetails));	
 }
}
