package com.hcl.shop.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.hcl.shop.entity.Order;
import com.hcl.shop.response.Response;
import com.hcl.shop.service.IOrderServices;

import io.swagger.annotations.ApiOperation;
@RestController
@CrossOrigin
public class OrderController {
	@Autowired
	private IOrderServices orderService;
	
//	@Autowired
//	OrderServiceImp orderServiceimpl;
	
	@PostMapping("shop/placeOrder")
	public ResponseEntity<Response> placeOrder(@RequestHeader String token,@RequestParam Long productId, @RequestParam Long addressId) throws Exception {
		Order orderdetails = orderService.placeOrder(token, productId, addressId);
		return ResponseEntity.status(HttpStatus.ACCEPTED).body(new Response(" CHECK YOUR MAIL  ORDER IS SUCCESSFULLY PLACED", 200, orderdetails));
	}
	
	@GetMapping(value = "shop/confirmProduct/{productId}")
	public ResponseEntity<Response> confirmProducttoOrder(@RequestHeader(name= "token") String token,@PathVariable("productId") long productId) throws Exception {	
		boolean userdetails = orderService.confirmProducttoOrder(token, productId);
		return ResponseEntity.status(HttpStatus.ACCEPTED).body(new Response("order is placed", 200, userdetails));
	
		
	}
	
	@GetMapping(value = "/products/{token}")
	public ResponseEntity<Response> getOrderlist(@PathVariable("token") String token) throws Exception {
		
		List<Order> orderdetails = orderService.getOrderList(token);
			return ResponseEntity.status(HttpStatus.ACCEPTED).body(new Response("placed orderlist", 200, orderdetails));
		
	}
	@GetMapping(value = "/products_count/{token}")
	public ResponseEntity<Response> getProductsCount(@PathVariable("token") String token) throws Exception {
		
		int userdetails = orderService.getCountOfProducts(token);
			return ResponseEntity.status(HttpStatus.ACCEPTED).body(new Response("count of products", 200, userdetails));
		
	}
	
	

	
	@ApiOperation(value = "Change Order Status by admin ")
	@PutMapping(value = "shop/orderStatusByAdmin")
	public ResponseEntity<Response> changeOrderStausByAdmin(@RequestParam String status,@RequestParam long orderId,@RequestHeader("token") String token) throws Exception {
		
		int orderStatusResult = orderService.changeOrderStatus(status,orderId);
		System.out.println("orderStatusResult :"+orderStatusResult);
			return ResponseEntity.status(HttpStatus.OK).body(new Response(orderId+" order status updated ",200,orderStatusResult));
		
	}
	
	
	@ApiOperation(value = "get allorder details for admin")
	@GetMapping(value = "shop/getOrdersByAdmin")
	public ResponseEntity<Response> getallOrders() throws Exception {
		
		List<Order> orderinfo = orderService.getallOrders();
		System.out.println("order ids: "+orderinfo);
			return ResponseEntity.status(HttpStatus.OK).body(new Response(" orders list ",200,orderinfo));
		
	}
	
	@ApiOperation(value = "get In progress order details for seller")
	@GetMapping(value = "shop/getOrdersByseller")
	public ResponseEntity<Response> getInProgressOrders() throws Exception {
		System.out.println("------------Seller order");
		List<Order> orderinfo = orderService.getInProgressOrders();
		System.out.println("seller  ------order ids: "+orderinfo);
			return ResponseEntity.status(200).body(new Response(" orders list ",200,orderinfo));
		
	}
}