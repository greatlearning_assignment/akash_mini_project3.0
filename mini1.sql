
#Password = abcdef12

INSERT INTO shop.users(user_id,email,is_verified,mobile_number,name,password,role)
values (5,"pandey0203@gmail.com",TRUE,7509508624,"Yogesh Pandey","$2a$10$.tHUhjlq2UpAQ.jQHdEIe.OqyMh.RfJYeEuwXKNoeGllFkL9W9sb2","user");

INSERT INTO shop.users(user_id,email,is_verified,mobile_number,name,password,role)
values (6,"surabi123@gmail.com",TRUE,9876667889,"Surabi","$2a$10$.tHUhjlq2UpAQ.jQHdEIe.OqyMh.RfJYeEuwXKNoeGllFkL9W9sb2","seller");

INSERT INTO shop.users(user_id,email,is_verified,mobile_number,name,password,role)
values (7,"akash123@gmail.com",TRUE,9876667559,"Akash","$2a$10$.tHUhjlq2UpAQ.jQHdEIe.OqyMh.RfJYeEuwXKNoeGllFkL9W9sb2","admin");


INSERT INTO `shop`.`productinfo` (`product_id`, `image`, `no_of_products`, `price`, `product_author`, `product_details`, `product_name`, `status`, `user_id`) VALUES ('1', 'corn.jpg', '10', '199', 'Pizza', ' a dish made typically of flattened bread dough spread with a savory mixture usually including tomatoes and cheese and often other toppings and baked.', 'Butter Pizza', 'approved', '1');
INSERT INTO `shop`.`productinfo` (`product_id`, `created_date_and_time`, `image`, `no_of_products`, `price`, `product_author`, `product_details`, `product_name`, `status`, `user_id`) VALUES ('2', '2022-01-11 05:08:54.933667', 'pizza.jpg', '10', '99', 'Pizza', ' a dish made typically of flattened bread dough spread with a savory mixture usually including tomatoes and cheese and often other toppings and baked.', 'Cheese Pizza', 'approved', '1');
INSERT INTO `shop`.`productinfo` (`product_id`, `created_date_and_time`, `image`, `no_of_products`, `price`, `product_author`, `product_details`, `product_name`, `status`, `user_id`) VALUES ('3', '2022-01-11 05:08:54.933667', 'Burger.jpg', '10', '199', 'Burger', 'A hamburger (or burger for short) is a food, typically considered a sandwich, consisting of one or more cooked patties—usually ground meat, typically beef—placed inside a sliced bread roll or bun. The patty may be pan fried, grilled, smoked or flame broiled. ... A hamburger topped with cheese is called a cheeseburger.', 'Burger', 'approved', '1');

