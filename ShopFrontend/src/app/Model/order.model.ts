export class Order {
    productAuthor: string;
    productName: string;
    productDetails: string;
    image: string;
    totalprice: number;
    orderId: number;
    orderStatus: string;
}

