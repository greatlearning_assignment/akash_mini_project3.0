import { ComponentFixture, TestBed } from '@angular/core/testing';

import { UploadproductimageComponent } from './uploadproductimage.component';

describe('UploadproductimageComponent', () => {
  let component: UploadproductimageComponent;
  let fixture: ComponentFixture<UploadproductimageComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ UploadproductimageComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(UploadproductimageComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
